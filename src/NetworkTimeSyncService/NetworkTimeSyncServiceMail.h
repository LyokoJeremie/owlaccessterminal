// jeremie

#ifndef OWLACCESSTERMINAL_NETWORKTIMESYNCSERVICEMAIL_H
#define OWLACCESSTERMINAL_NETWORKTIMESYNCSERVICEMAIL_H

#include "../MemoryBoost.h"
#include <functional>
#include <chrono>
#include <vector>
#include "../AsyncCallbackMailbox/AsyncCallbackMailbox.h"

namespace OwlMailDefine {

    enum class NetworkTimeSyncServiceCmd {
        ignore = 0,
        getClock,
        getSyncRecord,
    };

    struct SyncRecordInfo : public boost::enable_shared_from_this<SyncRecordInfo> {
        std::vector<int64_t> syncTimeMs;
        std::vector<int64_t> syncDelayMs;
    };

    struct NetworkTimeSync2Service;
    struct Service2NetworkTimeSync {
        NetworkTimeSyncServiceCmd cmd = NetworkTimeSyncServiceCmd::ignore;

        // NetworkTimeSync2Service.runner = Service2NetworkTimeSync.callbackRunner
        std::function<void(boost::shared_ptr<NetworkTimeSyncServiceCmd>)> callbackRunner;
    };
    struct NetworkTimeSync2Service {

        int64_t steadyClockTimestampMs = 0;
        int64_t syncClockTimestampMs = 0;
        int64_t lastSyncTimeMs = -1;
        int64_t lastSyncDelayMs = -1;

        boost::shared_ptr<SyncRecordInfo> syncRecord{nullptr};

        std::function<void(boost::shared_ptr<NetworkTimeSyncServiceCmd>)> runner;
    };
    using ServiceNetworkTimeSyncMailbox =
            boost::shared_ptr<
                    OwlAsyncCallbackMailbox::AsyncCallbackMailbox<
                            Service2NetworkTimeSync,
                            NetworkTimeSync2Service
                    >
            >;

    using MailService2NetworkTimeSync = ServiceNetworkTimeSyncMailbox::element_type::A2B_t;
    using MailNetworkTimeSync2Service = ServiceNetworkTimeSyncMailbox::element_type::B2A_t;

}


#endif //OWLACCESSTERMINAL_NETWORKTIMESYNCSERVICEMAIL_H
