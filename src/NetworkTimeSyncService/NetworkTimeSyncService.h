// jeremie

#ifndef OWLACCESSTERMINAL_NETWORKTIMESYNCSERVICE_H
#define OWLACCESSTERMINAL_NETWORKTIMESYNCSERVICE_H

#include "../MemoryBoost.h"
#include <vector>
#include <list>
#include <utility>
#include <functional>
#include <chrono>
#include <atomic>
#include <boost/asio.hpp>
#include "../OwlLog/OwlLog.h"
#include "./NetworkTimeSyncServiceMail.h"

namespace OwlNetworkTimeSyncService {

    // (rSend)[rC1] -> (lRecord) rC1, (lRead) lC1, (lCalc) diff1 -> (lSend)[rC1,lC1,diff1] -> (rSend)[rC1,lC1,diff1,rC2]
    //       -> (lRecord) rC1,lC1,diff1,rC2, (lRead) lC2, (lCalc) diff2,diff3, (lCalc) delay1,delay2, (lCalc) diff0
    // -----------------------------------------------------------
    // remote ======>> local rC1 lC1 diff1
    // remote <<====== local rC1 lC1 diff1 rC2
    // remote ======>> local rC1 lC1 diff1 rC2 diff2 lC2 diff3
    // local 000000000 calc  delay1 delay2 diff0
    // -----------------------------------------------------------
    // the remote is controller , the local is Airplane
    // if rC==lC , the diff=(receiver-sender)=delay>0
    struct NetworkTimeSyncingInfo {
        // p1 receive from remote
        int64_t remoteClockMs1{0};
        // p1 read from local
        int64_t localClockMs1{0};
        // p1  localClockMs1 - remoteClockMs1
        int64_t clockDiffMs1{0};

        // p2 receive from remote
        int64_t remoteClockMs2{0};
        // p2 read from local
        int64_t localClockMs2{0};
        // p2  localClockMs2 - remoteClockMs2
        int64_t clockDiffMs2{0};

        // p2  remoteClockMs2 - localClockMs1
        int64_t clockDiffMs12{0};

        // (clockDiffMs1+clockDiffMs2)/2 == (remoteClockMs2 - localClockMs1(d) + localClockMs1(d) - remoteClockMs1 + (delay))
        // average up-stream offset
        int64_t delayMs1{0};
        // (clockDiffMs2+clockDiffMs12)/2 == (localClockMs2(d) - remoteClockMs2 + remoteClockMs2 - localClockMs1(d) + (delay))
        int64_t delayMs2{0};
        // TODO to predicate remote clock when local received package2
        int64_t clockDiffMs0{0};
    };

    class NetworkTimeSyncService : public boost::enable_shared_from_this<NetworkTimeSyncService> {
    public:
        NetworkTimeSyncService(
                boost::asio::io_context &ioc,
                std::vector<OwlMailDefine::ServiceNetworkTimeSyncMailbox> &&mailbox_list
        ) : ioc_(ioc),
            mailbox_list_(std::move(mailbox_list)) {
            for (auto &mailbox: mailbox_list_) {
                mailbox->receiveA2B([this, &mailbox](OwlMailDefine::MailService2NetworkTimeSync &&data) {
                    processMail(std::move(data), mailbox);
                });
            }
        }

        ~NetworkTimeSyncService() {
            BOOST_LOG_OWL(trace_dtor) << "~TimeService()";
            stop = true;
            for (auto &mailbox: mailbox_list_) {
                mailbox->receiveA2B(nullptr);
            }
        }

    private:
        boost::asio::io_context &ioc_;
        std::vector<OwlMailDefine::ServiceNetworkTimeSyncMailbox> mailbox_list_;

        std::atomic_bool stop = false;

        std::atomic_int64_t timeDiffMs{0};

        // (sync time point) --- (sync network delay)
        std::pair<std::list<int64_t>, std::list<int64_t>> syncHistory;

    public:
        int64_t getNowSteadyClock() {
            auto nowT = std::chrono::steady_clock::now();
            return std::chrono::time_point_cast<std::chrono::milliseconds>(nowT).time_since_epoch().count();
        }

        int64_t getNowSyncClock() {
            auto nowT = std::chrono::steady_clock::now();
            auto syncT = nowT - std::chrono::milliseconds{timeDiffMs.load()};
            return std::chrono::time_point_cast<std::chrono::milliseconds>(syncT).time_since_epoch().count();
        }

    private:
        void processMail(
                OwlMailDefine::MailService2NetworkTimeSync &&data,
                OwlMailDefine::ServiceNetworkTimeSyncMailbox mailbox
        ) {
            if (stop)return;
            boost::asio::dispatch(ioc_, [this, self = shared_from_this(), data, mailbox]() {
                if (stop)return;
                switch (data->cmd) {
                    case OwlMailDefine::NetworkTimeSyncServiceCmd::getClock: {
                        auto data_r = boost::make_shared<OwlMailDefine::NetworkTimeSync2Service>();
                        data_r->runner = data->callbackRunner;
                        data_r->steadyClockTimestampMs = getNowSteadyClock();
                        data_r->syncClockTimestampMs = getNowSyncClock();
                        sendBackMail(mailbox, std::move(data_r));
                    }
                        break;
                    case OwlMailDefine::NetworkTimeSyncServiceCmd::getSyncRecord: {
                        auto data_r = boost::make_shared<OwlMailDefine::NetworkTimeSync2Service>();
                        data_r->runner = data->callbackRunner;
                        sendBackMail(mailbox, std::move(data_r));
                    }
                        break;
                    case OwlMailDefine::NetworkTimeSyncServiceCmd::ignore:
                    default: {
                        BOOST_LOG_OWL(error)
                            << "NetworkTimeSyncService receiveMail switch(data->cmd) NetworkTimeSyncServiceCmd ignore..";
                        auto data_r = boost::make_shared<OwlMailDefine::NetworkTimeSync2Service>();
                        data_r->runner = data->callbackRunner;
                        sendBackMail(mailbox, std::move(data_r));
                        break;
                    }
                };
            });
        }

        void sendBackMail(
                const OwlMailDefine::ServiceNetworkTimeSyncMailbox &mailbox,
                OwlMailDefine::MailNetworkTimeSync2Service &&data) {
            mailbox->sendB2A(std::move(data));
        }

    };

} // OwlNetworkTimeSyncService

#endif //OWLACCESSTERMINAL_NETWORKTIMESYNCSERVICE_H
